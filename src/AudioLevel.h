//
// AudioLevel.h
//

#pragma once

int GetAudioVolume();
bool SetAudioVolume(int value);
