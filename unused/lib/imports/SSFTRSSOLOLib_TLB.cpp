// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 2-6-2016 16:17:31 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files (x86)\Clevy Dyscover\speech\components\common\ssftrssolo.ocx (1)
// LIBID: {6FEF776A-4CC5-11D4-9521-0000F8092E73}
// LCID: 0
// Helpfile: 
// HelpString: ScanSoft RealSpeak Solo ActiveX Control module
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "SSFTRSSOLOLib_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Ssftrssololib_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
const GUID LIBID_SSFTRSSOLOLib = {0x6FEF776A, 0x4CC5, 0x11D4,{ 0x95, 0x21, 0x00,0x00, 0xF8, 0x09,0x2E, 0x73} };
const GUID DIID__DSsftRSSolo = {0x6FEF776B, 0x4CC5, 0x11D4,{ 0x95, 0x21, 0x00,0x00, 0xF8, 0x09,0x2E, 0x73} };
const GUID DIID__DSsftRSSoloEvents = {0x6FEF776C, 0x4CC5, 0x11D4,{ 0x95, 0x21, 0x00,0x00, 0xF8, 0x09,0x2E, 0x73} };
const GUID CLSID_SsftRSSoloA = {0x6FEF776D, 0x4CC5, 0x11D4,{ 0x95, 0x21, 0x00,0x00, 0xF8, 0x09,0x2E, 0x73} };

};     // namespace Ssftrssololib_tlb
