Clevy
=====

This repository contains source code of Clevy Dyscover.

Dependencies
------------

- C++ compiler that supports C++14
- CMake (version 3.10 or above)
- wxWidgets (version 3.0 or above)

Building
--------

```
mkdir build
cd build
cmake ..
cmake --build .
```
