
Translations
------------

To update translation template and all the existing translations, run:

$ ./update.sh

To create translation for language `XX`, run:

$ msginit  --no-translator  --input=Clevy.pot  --locale=XX
